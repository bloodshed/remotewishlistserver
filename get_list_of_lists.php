<?php

$link = mysqli_connect('localhost', 'Margarita', '142536', 'remote_wish_list_db');

$id = $_GET['id'];

$response = array();

$query = "SELECT * FROM list_of_lists_tab";

$result = mysqli_query($link, $query);

$response["items"] = array();


if (mysqli_num_rows($result) > 0) {

    $items = array();
    while ($row = mysqli_fetch_array($result)) {

        $items = array();
        $items["id"] = $row["id"];
        $items["title"] = $row["title"];
        $items["description"] = $row["description"];
        $items["numberOfElements"] = $row["numberOfElements"];
        array_push($response["items"], $items);
    }
}

echo json_encode($response);